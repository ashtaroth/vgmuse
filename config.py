from __main__ import app

app.config['DEBUG'] = False
app.config['MPD_GRACETIME'] = 0
app.config['MPD_HOST'] = '127.0.0.1'
app.config['MPD_PORT'] = 6600
app.config['MPD_PASSWORD'] = 'changeme'
app.config['SOCKETIO_HOST'] = '127.0.0.1'
app.config['SOCKETIO_PORT'] = 5002
app.config['VGM_BOXART_DIR'] = ''
app.config['VGM_BOXART_FILENAME'] = ''
app.config['VGM_BOXART_DEFAULTIMAGE'] = ''
