# Setup

## Installing Requirements

    pip install Flask
    pip install python-mpd2
    pip install gevent-socketio

# Note

This software hasn't been deployed anywhere yet and has no security features  
whatsoever. DO NOT deploy this on public servers without extra protection.
