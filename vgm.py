import base64
import time
from flask import Flask
from flask.ext.socketio import SocketIO, emit
from threading import Thread
from json import dumps as json
from mpd import MPDClient
from time import sleep


class Client(MPDClient):


    def __init__(self, *args, **kwargs):

        kwargs["use_unicode"] = True
        super(Client, self).__init__(*args, **kwargs)
        self.connect(host=app.config["MPD_HOST"], port=app.config["MPD_PORT"])

        if app.config.has_key("MPD_PASSWORD") and app.config["MPD_PASSWORD"] != False:
            self.password(app.config["MPD_PASSWORD"])


    def get_json(self):
	
        album = ""
        title = ""
        musicbrainz_albumid = ""
        image = app.config["VGM_BOXART_DEFAULT_IMAGE_BASE64"]

        song_info = self.currentsong()

        if song_info.has_key("album"):
            album = song_info["album"]

        if song_info.has_key("title"):
            title = song_info["title"]

        if song_info.has_key("musicbrainz_albumid"):
            musicbrainz_albumid = song_info["musicbrainz_albumid"]

        try:
            fileName = app.config["VGM_BOXART_DIR"] + song_info["file"].split("/")[0] + app.config["VGM_BOXART_IMAGE_FILENAME"]

            with open(fileName, "rb") as image_file:
                image = base64.b64encode(image_file.read())

        except (IOError):
            pass

        game = { "name": album, "image": "data:image/png;base64," + image }
        song = { "name": title, "duration": self.currentsong()["time"], "elapsed": self.status()["elapsed"] }
        file = { "type": self.currentsong()["file"].rsplit(".", 1)[1].upper(), "musicbrainz_albumid": musicbrainz_albumid }

        result = "{\"game\":" + json(game) + ", \"song\":" + json(song) + ", \"file\":" + json(file) + "}"

        return result



app = Flask(__name__)
import config

socketio = SocketIO(app)
thread = None
client = None
listeners = 0



@socketio.on("connect")
def handle_connect():	
	
    global listeners, thread

    listeners += 1
    socketio.emit("listenercount_updated", json({"count" : listeners}))

    cli = Client()
    socketio.emit("connection_established", cli.get_json())
    cli.disconnect()

    if thread is None:
        thread = Thread(target=background_thread)
        thread.daemon = True
        thread.start()


@socketio.on("disconnect")
def handle_disconnect():

	global listeners
	listeners -= 1
	socketio.emit("listenercount_updated", json({"count" : listeners}))


def background_thread():

    global client
    client = Client()

    while True:

        try:
            client.idle("player")
            payload = client.get_json()
            socketio.emit("songinfo_updated", payload)

        except Exception as err:
            print "Unexpected error:", sys.exc_info()[0]

if __name__ == "__main__":
    socketio.run(app, host=app.config["SOCKETIO_HOST"], port=app.config["SOCKETIO_PORT"])
